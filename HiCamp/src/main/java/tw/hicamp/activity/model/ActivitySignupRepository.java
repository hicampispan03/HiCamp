package tw.hicamp.activity.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivitySignupRepository extends JpaRepository<ActivitySignup, Integer> {


}
