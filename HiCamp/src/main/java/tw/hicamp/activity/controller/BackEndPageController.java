package tw.hicamp.activity.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import tw.hicamp.activity.dto.ActivityDto;
import tw.hicamp.activity.dto.ActivityDtoForBackEndPage;
import tw.hicamp.activity.model.Activity;
import tw.hicamp.activity.model.ActivityPeriod;
import tw.hicamp.activity.model.ActivityPicture;
import tw.hicamp.activity.service.ActivityPeriodService;
import tw.hicamp.activity.service.ActivityPictureService;
import tw.hicamp.activity.service.ActivityService;

@Controller
public class BackEndPageController {

	@Autowired
	private ActivityService aService;

	@Autowired
	private ActivityPictureService actPicService;

	@Autowired
	private ActivityPeriodService actPeriodService;

//	for test only 

	@GetMapping("/activity/testtest")
	public String testtest() {
		return "activity/testpage";
	}

// ==新增==========================================================================================	

//	新增活動資訊(含照片&期別) postman
	@PostMapping("/activity/insertActivity")
	public String InsertActivityPicPeriod(@RequestParam("activityType") String activityType,
			@RequestParam("activityName") String activityName,
			@RequestParam("activityLocation") String activityLocation,
			@RequestParam("activityInfo") String activityInfo, @RequestParam("activityQuota") Integer activityQuota,
			@RequestParam("activityPrice") Integer activityPrice,

			@RequestParam("activityPicture") MultipartFile[] files,

			@RequestParam("activityDepartureDate") Date activityDepartureDate,
			@RequestParam("activityReturnDate") Date activityReturnDate,
			@RequestParam("signupDeadline") Date signupDeadline, 
			@RequestParam("signupQuantity") Integer signupQuantity,
			@RequestParam("activityPeriodQuota") Integer activityPeriodQuota,
			@RequestParam("activityPeriodPrice") Integer activityPeriodPrice,

			Model model) throws IOException {

		Activity activity = new Activity();

		activity.setActivityType(activityType);
		activity.setActivityName(activityName);
		activity.setActivityLocation(activityLocation);
		activity.setActivityInfo(activityInfo);
		activity.setActivityQuota(activityQuota);
		activity.setActivityPrice(activityPrice);

		List<ActivityPicture> activityPictureList = new ArrayList<>();

		for (MultipartFile file : files) {
			ActivityPicture actPicture = new ActivityPicture();
			byte[] actPictureByte = file.getBytes();
			actPicture.setActivityFileName(file.getOriginalFilename());
			actPicture.setActivityPicture(actPictureByte);
			actPicture.setActivity(activity);

			activityPictureList.add(actPicture);
		}

		List<ActivityPeriod> activityPeriodList = new ArrayList<>();

		ActivityPeriod activityPeriod = new ActivityPeriod();

		activityPeriod.setActivityDepartureDate(activityDepartureDate);
		activityPeriod.setActivityReturnDate(activityReturnDate);
		activityPeriod.setSignupDeadline(signupDeadline);
		activityPeriod.setSignupQuantity(signupQuantity);
		activityPeriod.setActivityPeriodQuota(activityPeriodQuota);
		activityPeriod.setActivityPeriodPrice(activityPeriodPrice);
		activityPeriod.setActivity(activity);

		activityPeriodList.add(activityPeriod);

		activity.setActivityPictures(activityPictureList);
		activity.setActivityPeriods(activityPeriodList);

		aService.insert(activity);

		return "redirect:/activity/allDataInBackEndPage";
	}

// ==查詢==========================================================================================

//	查詢全部(DTO) postman測試ok html還沒寫
	@GetMapping("/activity/allDataInBackEndPage")
	public String findAllActivityWithPictureAndPeriod(Model model) {

		List<Activity> activityList = aService.findAllActivity();
		
		List<ActivityPeriod> activityPeriodList = actPeriodService.findAllActPeriods();

		List<ActivityDtoForBackEndPage> dtos = new ArrayList<>();

		for (Activity activity : activityList) {
			ActivityDtoForBackEndPage activityDto = new ActivityDtoForBackEndPage();
			activityDto.setActivityNo(activity.getActivityNo());
			activityDto.setActivityType(activity.getActivityType());
			activityDto.setActivityName(activity.getActivityName());
			
//			Integer activityNo = actPicService.findByActivityNo(activity.getActivityNo());
//			activityDto.setActivityPicNo(activityNo);

			for (ActivityPeriod activityPeriod : activityPeriodList) {
		        if (activityPeriod.getActivityNo().equals(activity.getActivityNo())) {
		            activityDto.setActivityPeriodNo(activityPeriod.getActivityPeriodNo());
		            activityDto.setActivityDepartureDate(activityPeriod.getActivityDepartureDate());
		            activityDto.setActivityReturnDate(activityPeriod.getActivityReturnDate());
		            activityDto.setSignupDeadline(activityPeriod.getSignupDeadline());
		            activityDto.setSignupQuantity(activityPeriod.getSignupQuantity());
		            activityDto.setActivityPeriodQuota(activityPeriod.getActivityPeriodQuota());
		            activityDto.setActivityPeriodPrice(activityPeriod.getActivityPeriodPrice());
		            break;  
		        }
	        }
			dtos.add(activityDto);
		}

		System.out.println("dtos" + dtos);
		model.addAttribute("activity", dtos);

		return "activity/activityAllDatas";

	}

//	查詢活動資訊(含照片&期別) postman ok html not yet
	@GetMapping("/activity/findActivityWithPicPeriodByActivityNo")
	public String findOneActivityWithPictureAndPeriod(@RequestParam("activityNo") Integer activityNo, Model model) {

		Activity activity = aService.findActivityByActId(activityNo);
		List<ActivityPicture> activityPictures = activity.getActivityPictures();
		List<ActivityPeriod> activityPeriods = activity.getActivityPeriods();

		model.addAttribute("activity", activity);
		model.addAttribute("activityPictures", activityPictures);
		model.addAttribute("activityPeriods", activityPeriods);

		return "activity/testFindById";
		
	}

//  透過活動期別查詢資料  POSTMAN TEST OK 
	@GetMapping("/activity/findActivityPeriodById")
	public String findActivityByActivityPeriod(@RequestParam("activityPeriodNo") Integer activityPeriodNo, Model model) {
		
		ActivityPeriod activityPeriod = actPeriodService.findActPeriodById(activityPeriodNo);
		
		model.addAttribute("activityPeriod", activityPeriod);
		return "activity/testActivityPeriod";
	}
	
// ==修改==========================================================================================	

//	修改活動 POSTMAN TEST  "activityDepartureDate":"2023-06-26T00:00:00+08:00" OK  HTML還沒寫
	@ResponseBody
	@PutMapping("/activity/updateActivityDto")
	public String updateActivityWithPeriod(@RequestBody ActivityDtoForBackEndPage activity, Model model) {
//
//		aService.updateActivityDtoById(activity.getActivityNo(), activity.getActivityType(), activity.getActivityName(),
//				activity.getActivityLocation(), activity.getActivityInfo());

		actPeriodService.updateActivityPeriodById(activity.getActivityPeriodNo(), activity.getActivityNo(),
				activity.getActivityDepartureDate(), activity.getActivityReturnDate(), activity.getSignupDeadline(),
				activity.getSignupQuantity(), activity.getActivityPeriodPrice(), activity.getActivityPeriodQuota());

		model.addAttribute("activity", activity);

		return "activity/testFindById";
	}

// ==刪除==========================================================================================
	
	@DeleteMapping("/activity/deleteActivityPeriod")
	public String deleteActivityPeriod(@RequestParam("activityPeriodNo") Integer activityPeriodNo) {
		actPeriodService.deleteActPeriodById(activityPeriodNo);
		return "刪除成功";
	}
	

}
