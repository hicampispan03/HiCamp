package tw.hicamp.activity.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import tw.hicamp.activity.model.Activity;
import tw.hicamp.activity.model.ActivityPicture;
import tw.hicamp.activity.service.ActivityPictureService;
import tw.hicamp.activity.service.ActivityService;

@Controller
public class ActivityPictureController {

	@Autowired
	private ActivityService aService;
	
	@Autowired
	private ActivityPictureService actPicService;
	
	// insert----------------------------------------------------------------------
	
	@GetMapping("/activity/picture")
	public String newPicture() {
		return "Activity/uploadPicture";
	}
	
	@PostMapping("activity/upload")
	public String uploadPicture(@RequestParam("activityFileName") String activityFileName, 
			@RequestParam("activityPicture") MultipartFile file) {
		
		try {
			List<ActivityPicture> activityPicture = (List<ActivityPicture>) new ActivityPicture();
			((ActivityPicture) activityPicture).setActivityFileName(activityFileName);
			((ActivityPicture) activityPicture).setActivityPicture(file.getBytes());
			
			actPicService.insertPictures(activityPicture);
			return "Activity/activityFindAll";
			
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/Activity/error";
		}
	}
	
	
	// 上傳照片(更新頁面編輯圖片) 要留
//		@PostMapping("/activity/insertpic")
//		public String uploadPicture(@RequestParam("activityNo") Integer activityNo,
//				@RequestParam("activityPicture") MultipartFile[] files, Model model) {
//
//			Activity activity = aService.findActivityById(activityNo);
//
//			if (activity == null) {
//				return "查無此筆資料";
//			}
//
//			try {
//				List<ActivityPicture> activityPictureList = new ArrayList<>();
//
//				for (MultipartFile file : files) {
//					ActivityPicture actPicture = new ActivityPicture();
//					actPicture.setActivityNo(activityNo);
//					actPicture.setActivityFileName(file.getOriginalFilename());
//					actPicture.setActivityPicture(file.getBytes());
//					actPicture.setActivity(activity);
//					activityPictureList.add(actPicture);
//				}
//
//				actPicService.insertPictures(activityPictureList);
//				aService.findActivityById(activityNo);
//				
//				model.addAttribute("activity", activity);
//				model.addAttribute("activityPictures", activityPictureList);
//
//				return "redirect:/activity/findByNo?activityNo=" + activityNo;
//			} catch (IOException e) {
//				e.printStackTrace();
//				return "上傳失敗";
//			}
//		}
//	
	// select----------------------------------------------------------------------
	
	
	
	// update----------------------------------------------------------------------
	
	
	// delete----------------------------------------------------------------------
	
}
