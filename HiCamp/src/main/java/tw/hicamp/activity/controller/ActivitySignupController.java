package tw.hicamp.activity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpSession;
import tw.hicamp.activity.service.ActivitySignupService;
import tw.hicamp.member.model.Member;
import tw.hicamp.member.service.MemberService;

@Controller
public class ActivitySignupController {
	
	@Autowired
	private ActivitySignupService actSignupService;
	
	@Autowired
	private MemberService memberService;
	
	
	
	@GetMapping("activity/test")
	public String testOrder() {
		return "activity/testOrders";
	}
	
//	報名活動 判斷會員是否已登入?
	@ResponseBody
	@PostMapping("activity/placeOrder")
	public String login(@RequestParam("memberEmail") String email,
						@RequestParam("memberPassword") String password,
						HttpSession session,
						Model m) {
		Member member = memberService.findByEmail(email);
		
	    if (member != null) {
	    	return "已登入";
//	    	這邊下訂單
	    	
			} else {
//			跳到登入頁面
		
		}
		return "activity/testOrders";
	}
	
	
	
	

	
	

	

}
