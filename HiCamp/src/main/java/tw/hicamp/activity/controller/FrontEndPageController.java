package tw.hicamp.activity.controller;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.Soundbank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.hicamp.activity.dto.ActivityDto;
import tw.hicamp.activity.model.Activity;
import tw.hicamp.activity.model.ActivityPeriod;
import tw.hicamp.activity.service.ActivityPeriodService;
import tw.hicamp.activity.service.ActivityPictureService;
import tw.hicamp.activity.service.ActivityService;

@Controller
public class FrontEndPageController {

	@Autowired
	private ActivityService aService;

	@Autowired
	private ActivityPictureService actPicService;

	@Autowired
	private ActivityPeriodService actPeriodService;
	
	
	
////查詢全部(Activity+ActivityPicture+ActivityPeriod) (後台管理)
//	@ResponseBody
//	@GetMapping("/activity/showAllData")
//	public String showActivityWithPictureAndPeriod(Model model) {
//
//		List<Activity> activityList = aService.findAllActivity();
//		List<ActivityPeriod> activityPeriodList = actPeriodService.findAllActPeriods();
//
//		List<ActivityDto> dtos = new ArrayList<>();
//
//		for (Activity activity : activityList) {
//			ActivityDto activityDto = new ActivityDto();
//			activityDto.setActivityNo(activity.getActivityNo());
//			activityDto.setActivityType(activity.getActivityType());
//			activityDto.setActivityName(activity.getActivityName());
//			activityDto.setActivityInfo(activity.getActivityInfo());
//			activityDto.setActivityQuota(activity.getActivityQuota());
//			activityDto.setActivityPrice(activity.getActivityPrice());
//
//			Integer activityNo = actPicService.findByActivityNo(activity.getActivityNo());
//			activityDto.setActivityPicNo(activityNo);
//			
//			for (ActivityPeriod activityPeriod : activityPeriodList) {
//				activityDto.setActivityDepartureDate(activityPeriod.getActivityDepartureDate());
//				activityDto.setActivityReturnDate(activityPeriod.getActivityReturnDate());
//				activityDto.setSignupDeadline(activityPeriod.getSignupDeadline());
//			}
//			
//			dtos.add(activityDto);
//			System.out.println(activityDto);
//
//		}
//		return "activity/activityAllDatas";
//
//
//	}

	
}
